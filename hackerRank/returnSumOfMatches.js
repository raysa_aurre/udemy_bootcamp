function sumMatch(x1, x2, y1, y2) {

    if (x2 < y1) {
        return 0;
    }

    if (x2 == y1) {
        return x2;
    }

    var result = 0;
    for (var i = x1; i <= x2; i++) {
        if (i == y1) {
            result += i;
            y1++;
        }
        continue;
    }

    return result;

}

console.log(sumMatch(5, 9, 10, 15));