function clock_angles(hours, minute) {
    var rateOfAngMin = minute * 6;
    var rateOfAngHr = 0.5 * (60 * hours + minute);
    angle = Math.abs(rateOfAngHr - rateOfAngMin);

    return angle;  
};

console.log(clock_angles(2, 20));