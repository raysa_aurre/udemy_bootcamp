function sortByLength(array) {
    
    function compare(a, b) {
        return a.length - b.length;
    };

   return array.sort(compare); 

};

console.log(sortByLength(["Telescopes", "Glasses", "Eyes", "Monocles"])); 
