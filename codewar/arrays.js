var nums = [45, 22, 18, 4, 10, 0, 1, 101, 92];
var words = ["abc", "ernesto", "raysa", "good", "smile", "happy"];

// Function to get the smallest number of nums array
function compare(a, b) {
    return a - b;
};

function smallestNumber(nums) {
    smallestNum = nums.sort(compare);
    return smallestNum[0];
};
console.log("The smmalest number is: ", smallestNumber(nums));

// Function to get the bigges number of nums array
function compare1(a, b) {
    return b - a;
};

function biggestNumber(nums) {
    biggestNum = nums.sort(compare1);
    return biggestNum[0];
}
console.log("The biggest number is:", biggestNumber(nums));

// Function to get the average  of nums array

function average(nums) {
    total = 0;
    for (var i = 0; i < nums.length; i++) {
        total += nums[i];
    }
    return total / nums.length;
};
console.log("The average is:", average(nums));


// Function to get the sum of nums array
function sum(nums) {
    total = 0;
    for (var i = 0; i < nums.length; i++) {
        total += nums[i];
    }
    return total;
}
console.log("The sum is:", sum(nums));

// Function to sort nums ascending 
function sortedAscending(nums) {
    return nums.sort(compare);
};
console.log("Array sorted ascending:", sortedAscending(nums));

// Function to sort nums descending
function sortedDescending(nums) {
    return nums.sort(compare1);
};
console.log("Array sorted descending:", sortedDescending(nums));

// Function to sort words alphabetically
function sortedAlphabetically(words) {
    return words.sort();
};
console.log("Array sorted alphabetically:", sortedAlphabetically(words));

// Function to sort words by elements length
function compareByLength(a, b) {
    return a.length - b.length;
};

function sortedByElementLength(words) {
    return words.sort(compareByLength);
};
console.log("Array sorted by elements length:", sortedByElementLength(words));

// Function to get element with less chars
function elementWithLessChars(words) {
    var newArr = words.sort(compareByLength);
    var result = [];
    for (var i = 0; i <= newArr.length - 1; i++) {
        if (newArr[i].length == newArr[0].length) {
            result.push(newArr[i]);
        }
    }
    return result;
};
console.log("The elements with less chars are:", elementWithLessChars(words));

// Function to get element with more chars
function compareByLeng(a, b) {
    return b.length - a.length;
};
function elementWithMoreChars(words) {
    var newArr = words.sort(compareByLeng);
    var result = [];
    for ( var i = 0; i <= newArr.length - 1; i++) {
        if ( newArr[i].length == newArr[0].length) {
            result.push(newArr[i]);
        }
    }
    return result;
};
console.log("The element with more chars is:", elementWithMoreChars(words));


